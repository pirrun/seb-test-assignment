package seb.sebtestassignment;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SebTestAssignmentApplication {

	public static void main(String[] args) {
		SpringApplication.run(SebTestAssignmentApplication.class, args);
	}

}
