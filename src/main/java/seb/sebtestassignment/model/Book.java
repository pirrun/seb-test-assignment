package seb.sebtestassignment.model;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.Data;
import lombok.ToString;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import java.util.ArrayList;
import java.util.List;

@Data
@Entity
@ToString(exclude = {"ratings"})
public class Book {

    @Id
    @Column(length = 13)
    private String isbn;

    @Column(length = 255)
    private String aBookTitle;

    @Column(length = 255)
    private String author;

    private Integer bookPublicationYear;

    @Column(length = 255)
    private String bookPublisher;

    @Column(length = 255)
    private String imageUrlA_S;

    @Column(length = 255)
    private String imageUrlB_M;

    @Column(length = 255)
    private String imageUrlC_L;

    @OneToMany(mappedBy = "book", cascade = CascadeType.ALL)
    @JsonManagedReference
    private List<Rating> ratings = new ArrayList<>();

    public double getRating(){
        return ratings.size() != 0 ? this.ratings.stream().mapToDouble(Rating::getRating).sum()/this.ratings.size() : 0;
    }
}
