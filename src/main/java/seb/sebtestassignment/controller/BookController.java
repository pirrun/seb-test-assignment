package seb.sebtestassignment.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import seb.sebtestassignment.model.Book;
import seb.sebtestassignment.service.BookService;

import static org.springframework.http.HttpStatus.OK;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("/book")
public class BookController {

    @Autowired
    private BookService bookService;

    @PostMapping("/create")
    public void createBook(Book book) {
        bookService.createBook(book);
    }

    @GetMapping("/{id}")
    public Book findById (String id) {
        return bookService.findById(id);
    }

    @PostMapping("/edit/{id}")
    public void updateBook (String id, Book book) {
        bookService.updateBook(id, book);
    }

    @GetMapping("/delete/{id}")
    public void deleteBook(String id) {
        bookService.deleteBook(id);
    }

    @GetMapping("/all")
    public ResponseEntity<?> allBooks(@PageableDefault Pageable pageable, @RequestParam(value = "top", required = false) int top) {
        if (top != 0) {
            return new ResponseEntity<>(bookService.getTopBooks(pageable, top), OK);
        } else {
            return new ResponseEntity<>(bookService.findAllBooks(pageable), OK);
        }
    }
}
