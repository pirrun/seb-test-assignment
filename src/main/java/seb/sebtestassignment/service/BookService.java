package seb.sebtestassignment.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import seb.sebtestassignment.model.Book;
import seb.sebtestassignment.repository.BookRepository;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
public class BookService {

    @Autowired
    private BookRepository bookRepository;

    public void createBook(Book book) {
        bookRepository.save(book);
    }

    public Book findById(String id) {
        return bookRepository.findById(id).orElse(null);
    }

    public void deleteBook(String id) {
        Book book = bookRepository.findById(id)
                .orElseThrow(() -> new IllegalArgumentException("Invalid book Id:" + id));
        bookRepository.delete(book);
    }

    public void updateBook(String id, Book updatedBook) {
        Book book = bookRepository.findById(id)
                .orElseThrow(() -> new IllegalArgumentException("Invalid book Id:" + id));
        updateAllFields(book, updatedBook);
        bookRepository.saveAndFlush(book);
    }

    public Page<Book> findAllBooks(Pageable page) {
        return bookRepository.findAll(page);
    }

    public Page<Book> getTopBooks(Pageable page, int topNumber) {
        List<Book> topBooks = bookRepository
                .findAll()
                .stream()
                .sorted(Comparator.comparing(Book::getRating).reversed())
                .limit(topNumber)
                .collect(Collectors.toList());
        return new PageImpl<>(topBooks);
    }

    private void updateAllFields(Book book, Book updatedBook) {
        book.setABookTitle(updatedBook.getABookTitle());
        book.setAuthor(updatedBook.getAuthor());
        book.setBookPublicationYear(updatedBook.getBookPublicationYear());
        book.setBookPublisher(updatedBook.getBookPublisher());
        book.setImageUrlA_S(updatedBook.getImageUrlA_S());
        book.setImageUrlB_M(updatedBook.getImageUrlB_M());
        book.setImageUrlC_L(updatedBook.getImageUrlC_L());
    }
}
